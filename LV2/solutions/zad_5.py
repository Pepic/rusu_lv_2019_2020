import numpy as np
import matplotlib.pyplot as plt
import pandas as p

potrosnje_automobila=[]
figure=plt.figure() 
plot=figure.add_subplot(1,1,1)
auti=p.read_csv("../resources/mtcars.csv")
plt.scatter(auti.mpg,auti.hp)
plt.title("Ovisnost potrosnje o konjskim snagama")
plt.xlabel("Potrosnja")
plt.ylabel("Konjske snage")
plt.grid(linestyle="-")

for i in range(0,len(auti.mpg)):
    plot.annotate("%.2f"%auti.wt[i],xy=(auti.mpg[i],auti.hp[i]),xytext=(auti.mpg[i],auti.hp[i]),arrowprops=dict(arrowstyle="-",facecolor="red"))
    potrosnje_automobila.append(auti.mpg[i])

plt.show()
average=np.average(potrosnje_automobila)
Min=min(potrosnje_automobila)
Max=max(potrosnje_automobila)
print("Srednja vrijednost:%.3f"%average," maksimalna:%.2f"%Max," minimalna:%.2f"%Min)