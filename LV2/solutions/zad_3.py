import numpy as np
import matplotlib.pyplot as plt

np.random.seed(10101111) 
rand_vector = np.random.randint(2, size=270)
height_m = []
height_f = []

for i in rand_vector:
    if i == 1:
        height_m.append(np.random.normal(167, 7))
    else:
        height_f.append(np.random.normal(180, 7))

plt.hist([height_m, height_f], color = ['blue', 'red'])

avg_m = np.average(height_m)
avg_f = np.average(height_f)

plt.axvline(avg_m, color='black')
plt.axvline(avg_f, color='green')
plt.legend(["avg_m", "avg_f", "male", "female"])