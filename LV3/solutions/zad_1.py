import pandas as pd
import numpy as np

mtcars=pd.read_csv('mtcars.csv')

# mtcars=mtcars.sort_values('mpg',ascending=False)  # Zadatak 1.1
# print(mtcars)
# print(mtcars.head(5))

# mtcars = mtcars.query('cyl == 8')   # Zadatak 1.2
# mtcars=mtcars.sort_values('mpg')
# print(mtcars.head(3))

# mtcars=mtcars.query('cyl == 6')    # Zadatak 1.3
# print(mtcars.mpg.mean())


# mtcars=mtcars.query('cyl == 4 & 2.000 <= wt & wt <= 2.200 ')   # Zadatak 1.4
# print(mtcars.mpg.mean())

# mtcars=mtcars.query('am == 0')         # Zadatak 1.5
# print(mtcars.am.count())
# mtcars=mtcars.query('am == 1')
# print(mtcars.am.count())

# mtcars=mtcars.query('am == 0 and hp > 100')          #  Zadatak 1.6
# print(mtcars.car.count())

# koef = 0.45359237                       # Zadatak 1.7
# mtcars = mtcars[['car','wt']]
# mtcars.wt *= koef*1000
# print(mtcars)
