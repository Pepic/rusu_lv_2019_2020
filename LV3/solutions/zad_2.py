import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')
print(mtcars)

# mtcars = mtcars.groupby('cyl').mean()      # Zadatak 2.1
# print(mtcars)
# plt.bar([4, 6, 8], mtcars['mpg'])
# plt.show()

# mtcars = mtcars[['cyl', 'wt']]             # Zadatak 2.2
# mtcars.boxplot(by='cyl')
# plt.show()

# mtcars = mtcars.groupby('am').mean()        # Zadatak 2.3
# print(mtcars)
# plt.bar([0, 1], mtcars['mpg'])
# plt.show()

# plt.scatter(mtcars[mtcars.am == 0]['qsec'], mtcars[mtcars.am == 0]['hp'], label='automatic', s=mtcars[mtcars.am == 0]['wt']*20)  # Zadatak 2.4
# plt.scatter(mtcars[mtcars.am == 1]['qsec'], mtcars[mtcars.am == 1]['hp'], label='manual', s=mtcars[mtcars.am == 1]['wt']*20)
# plt.legend(loc=1)
# plt.show()

