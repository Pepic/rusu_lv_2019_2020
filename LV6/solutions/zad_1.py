import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as sk
import sklearn.metrics as sm
import sklearn.neighbors as sn
from sklearn import preprocessing

def generate_data(n):

#prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2.0,(n1,1))
   # x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1, 2) + np.random.standard_normal((n1, 1))
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)

#druga klasa
    n2 = n - int(n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2)
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)

    data = np.concatenate((temp1,temp2),axis = 0)

#permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]

    return data


# Zadatak 1.
np.random.seed(242)
training_data = generate_data(200)
testing_data = generate_data(100)

# Zadatak 2.

training_data_0= training_data[training_data[:,2] == 0]
training_data_1 = training_data[training_data[:,2] == 1]

fig = plt.figure()

plt.scatter(x=training_data_0[:,0],y=training_data_0[:,1],c='green')
plt.scatter(x=training_data_1[:,0],y=training_data_1[:,1],c='blue')

# Zadatak 3.


model = sk.LogisticRegression()
model.fit(training_data[:,0:2],training_data[:,2])

b = model.intercept_[0]
w1, w2 = model.coef_.T


c = -b/w2
m = -w1/w2


xmin, xmax = -4, 4
ymin, ymax = -1, 25
xd = np.array([xmin, xmax])
yd = m*xd + c
plt.plot(xd, yd, 'k', lw=2, ls='--')


# Zadatak 4.

f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(training_data[:,0])-0.5:max(training_data[:,0])+0.5:.05,
 min(training_data[:,1])-0.5:max(training_data[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = model.predict_proba(grid)[:, 1].reshape(x_grid.shape)
cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')
plt.show()

# Zadatak 5.


predicted_class=model.predict(testing_data[:,0:2])
result=np.append(testing_data,predicted_class.reshape(predicted_class.shape[0],1),axis=1)


correct_predictions = result[result[:,2] == result[:,3]]
wrong_predictions = result[result[:,2] != result[:,3]]

plt.figure()
plt.scatter(x=correct_predictions[:,0],y=correct_predictions[:,1],c='green')
plt.scatter(x=wrong_predictions[:,0],y=wrong_predictions[:,1],c='black')

# Zadatak 6.

def plot_confusion_matrix(c_matrix):
 
 norm_conf = []
 for i in c_matrix:
     a = 0
     tmp_arr = []
     a = sum(i, 0)
     for j in i:
         tmp_arr.append(float(j)/float(a))
     norm_conf.append(tmp_arr)
 fig = plt.figure()
 ax = fig.add_subplot(111)
 res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
 width = len(c_matrix)
 height = len(c_matrix[0])
 
 for x in range(width):
     for y in range(height):
         ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                     horizontalalignment='center',
                     verticalalignment='center', color = 'green', size = 20)
 fig.colorbar(res)
 numbers = '0123456789'
 plt.xticks(range(width), numbers[:width])
 plt.yticks(range(height), numbers[:height])
 
 plt.ylabel('Stvarna klasa')
 plt.title('Predvideno modelom')
 plt.show()



conf_matrix = sm.confusion_matrix(result[:,2],result[:,3])
plot_confusion_matrix(conf_matrix)

# zadatak 7.


# from sklearn.preprocessing import PolynomialFeatures
# poly = PolynomialFeatures(degree=2, include_bias = False)
# training_data_new = poly.fit_transform(training_data[:,0:2])


# Zadatak 8.

knn_model = sn.KNeighborsClassifier(5)
preprocessing.scale(training_data[:,0:2])
knn_model.fit(training_data[:,0:2],training_data[:,2])



def plot_KNN(KNN_model, X, y):
 
 x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
 x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
 xx, yy = np.meshgrid(np.arange(x1_min, x1_max, 0.01),
 np.arange(x2_min, x2_max, 0.01))
 
 Z1 = KNN_model.predict(np.c_[xx.ravel(), yy.ravel()])
 Z = Z1.reshape(xx.shape)
 plt.figure()
 plt.pcolormesh(xx, yy, Z, cmap='PiYG', vmin = -2, vmax = 2)
 plt.scatter(X[:,0], X[:,1], c = y, s = 30, marker= 'o' , cmap='RdBu', edgecolor='white', label = 'train')


plot_KNN(knn_model,training_data[:,0:2],training_data[:,2])












