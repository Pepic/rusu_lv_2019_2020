import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix


# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# TODO: prikazi nekoliko slika iz train skupa
plt.figure()
plt.imshow(x_train[0],cmap='gray')
plt.figure()
plt.imshow(x_train[1],cmap='gray')
plt.figure()
plt.imshow(x_train[2],cmap='gray')
plt.figure()
plt.imshow(x_train[3],cmap='gray')

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1 )

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu

model = keras.Sequential()
model.add(keras.Input(input_shape))
model.add(layers.Flatten())
model.add(layers.Dense(32, activation = 'relu', ))
model.add(layers.Dense(num_classes,activation = 'softmax'))
model.summary()

# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])


# TODO: provedi ucenje mreze
epochs = 8
batch_size = 64

model.fit(x_train_s, y_train_s,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test_s, y_test_s))


# TODO: Prikazi test accuracy i matricu zabune

score_test = model.evaluate(x_test_s, y_test_s, verbose=0)
print('Test loss:', score_test[0])
print('Test accuracy:', score_test[1])


score_train = model.evaluate(x_train_s, y_train_s, verbose=0)
print('Train loss:', score_train[0])
print('Train accuracy:', score_train[1])

classes = model.predict(x_test_s, batch_size=batch_size)
y_pred = (classes > 0.5)
confusionMatrix = confusion_matrix(y_test_s.argmax(axis=1), y_pred.argmax(axis=1))
print(confusionMatrix)

# TODO: spremi model
model.save("sequential_model")



# model s konvolucijom



model_conv = keras.Sequential()
model_conv.add(keras.Input(input_shape))
model_conv.add(layers.Conv2D(32, kernel_size=(3, 3),
     activation='relu'))
model_conv.add(layers.Conv2D(64, (3, 3), activation='relu'))
model_conv.add(layers.MaxPooling2D(pool_size=(2, 2)))
model_conv.add(layers.Dropout(0.25))
model_conv.add(layers.Flatten())
model_conv.add(layers.Dense(32, activation = 'relu', ))
model_conv.add(layers.Dropout(0.5))
model_conv.add(layers.Dense(num_classes,activation = 'softmax'))
model_conv.summary()

model_conv.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
model_conv.fit(x_train_s, y_train_s,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test_s, y_test_s))

scoreTest = model_conv.evaluate(x_test_s, y_test_s, verbose=0)
print('Test loss:', scoreTest[0])
print('Test accuracy:', scoreTest[1])

scoreTrain = model_conv.evaluate(x_train_s, y_train_s, verbose=0)
print('Train loss:', scoreTrain[0])
print('Train accuracy:', scoreTrain[1])

classes_conv = model_conv.predict(x_test_s, batch_size=batch_size)
y_pred_conv = (classes_conv > 0.5)

confusionMatrix_conv = confusion_matrix(y_test_s.argmax(axis=1), y_pred_conv.argmax(axis=1))
print(confusionMatrix_conv)

model.save("sequential_model_conv")

