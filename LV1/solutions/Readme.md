Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.

Izmjene napravljene u python skripti:

1.Funkciju raw_input zamjeniti s funkcijom input.
2.Parametri funkcije print moraju biti unutar zagrada
3.Varijablu fnamex preimenovati u fname
4.U for petlji, u else grani staviti += umjesto = kako bi se brojao ukupan broj ponavljanja riječi