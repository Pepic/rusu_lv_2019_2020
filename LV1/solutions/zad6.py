fname = input('Enter the file name: ')
fhand = open(fname)
counts = dict()
email_list = list()
counts = dict()

for line in fhand:
    if line.startswith("From"):
        words = line.split()
        for word in words:
            if "@" not in word:
                continue
            else:
                if word not in email_list:
                    email_list.append(word)
    else:
        continue

for email in email_list:
    if email.split("@")[1] not in counts:
        counts[email.split("@")[1]] = 1
    else:
        counts[email.split("@")[1]] += 1
        
print(counts)