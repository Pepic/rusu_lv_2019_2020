try:
    number=float(input("Unesite broj između 0.0 i 1.0: "))
except Exception:
    print("Niste unjeli broj!")
    exit()

if(number>=0.9 and number<=1): 
    print("A")
elif(number>=0.8 and number<0.9): 
    print("B")
elif(number>=0.7 and number<0.8): 
    print("C")
elif(number>=0.6 and number<0.7): 
    print("D")
elif(number>=0.0 and number<0.6): 
    print("F")
else:
    print("Broj nije unutar zadanog intervala!")