fname = input("Enter file name: ")
file = open(fname)
count=0
total=0
string="X-DSPAM-Confidence:"

for line in file:
    
    if line.startswith("X-DSPAM-Confidence:"):
        count=count+1
        num=float(line[len(string)+1:])
        total=total+num
        
    else:
        continue

print("Average X-DSPAM-Confidence:",float(total/count))