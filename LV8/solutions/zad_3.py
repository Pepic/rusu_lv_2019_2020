from tensorflow import keras
from tensorflow.keras import layers


datasetTest = keras.utils.image_dataset_from_directory('Test_Sorted',image_size=(48,48))
datasetTrain = keras.utils.image_dataset_from_directory('Train',image_size=(48,48))


num_classes = 43
input_shape = (48, 48, 3)

model = keras.Sequential()
model.add(keras.Input(input_shape))
model.add(layers.Conv2D(32, kernel_size=(3, 3), activation='relu'))
model.add(layers.Conv2D(32, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Dropout(0.2))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Dropout(0.2))
model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Dropout(0.2))
model.add(layers.Flatten())
model.add(layers.Dense(512, activation = 'relu', ))
model.add(layers.Dropout(0.5))
model.add(layers.Dense(num_classes,activation = 'softmax'))
model.summary()

model.compile(loss='sparse_categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])



model.fit(datasetTrain,
          batch_size=64,
          epochs=5)
          

scoreTest = model.evaluate(datasetTest)